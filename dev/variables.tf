# === root/variables.tf ===
# 
# When everything is set, the root config is the config we'll be making changes in

# Our environment, used in names and such
variable "env" {
  default = "dev"
}

# Our image map, dividing our different images and environments
variable "image" {
  type = map(any)
  default = {
    nginx = {
      dev = "nginx:latest"
      tst = "nginx:latest"
      acc = "nginx:latest"
      prd = "nginx:stable"
    }
    grafana = {
      dev = "grafana/grafana:latest"
      tst = "grafana/grafana:latest"
      acc = "grafana/grafana:latest"
      prd = "grafana/grafana:latest-ubuntu"
    }
  }
}

# Internal port (just as reference in our local block and for_each)
variable "int_port" {
  type    = number
  default = 9999
}

# External port (just as reference in our local block and for_each)
variable "ext_port" {
  type = map(any)
}
