# === root/outputs.tf ===
# 
# When everything is set, the root config is the config we'll be making changes in

# I've cleaned this up a lot while making the code more DRY
output "app_access" {
  # Name and docket for each application
  value = [for i in module.container[*] : i]
}
