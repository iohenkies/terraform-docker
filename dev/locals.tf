# === root/locals.tf ===
# 
# When everything is set, the root config is the config we'll be making changes in

# This one is very important for the for_each function in main.tf, making our deployment more DRY
locals {
  # Name is arbitrary
  deployment = {
    nginx = {
      # These keys will be used later as reference
      container_count = length(var.ext_port["nginx"][var.env])
      image           = var.image["nginx"][var.env]
      int             = 80
      ext             = var.ext_port["nginx"][var.env]
      # For our dynamic block we're creating a list with maps in there, so that we can create multiple volumes when needed
      volumes = [
        { container_path_each = "/usr/share/nginx/html" }
      ]
    }
    grafana = {
      # These keys will be used later as reference
      container_count = length(var.ext_port["grafana"][var.env])
      image           = var.image["grafana"][var.env]
      int             = 3000
      ext             = var.ext_port["grafana"][var.env]
      # For our dynamic block we're creating a list with maps in there, so that we can create multiple volumes when needed
      volumes = [
        { container_path_each = "/var/lib/grafana" },
        { container_path_each = "/etc/grafana" }
      ]
    }
  }
}
