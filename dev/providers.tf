# === root/providers.tf ===
# 
# When everything is set, the root config is the config we'll be making changes in

# Source has been changed since version 0.14
terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}

# Docker provider config is default, hence the {}
provider "docker" {}
