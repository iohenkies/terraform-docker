# === root/terraform.tfvars ===
# 
# When everything is set, the root config is the config we'll be making changes in

# The amount of ports is directly related to the amount of containers, see the container_count in the root main.tf file
ext_port = {
  nginx = {
    dev = [8080]
    tst = [7070]
    acc = [6060]
    prd = [9090, 9191, 9292]
  }
  grafana = {
    dev = [3100, 3200]
    tst = [3300]
    acc = [3400]
    prd = [3000]
  }
}
