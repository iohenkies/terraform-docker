# === root/main.tf ===
# 
# When everything is set, the root config is the config we'll be making changes in

# Links to the image child module
module "image" {
  source = "../modules/image"
  # With this for each we're making our config more general and DRY. We're using the local from the locals.tf file
  for_each = local.deployment
  image_in = each.value.image
}

# Links to the container child module
module "container" {
  source   = "../modules/container"
  env_in   = var.env
  count_in = each.value.container_count
  # Here are our variables, being passed along to our container child module
  for_each    = local.deployment
  name_in     = each.key
  image_in    = module.image[each.key].image_out
  int_port_in = each.value.int
  ext_port_in = each.value.ext
  volumes_in  = each.value.volumes
}
