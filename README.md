# Introduction 

First Terraform project, deploying a couple of test Docker containers and persistent volumes.

It has become a dynamic, modular, production ready project, featuring very dry code, that you can use as a basis for your Docker projects that you want to rollout with Terraform.

All the config files are heavily commented, explaining almost every line of code. Globally we have:
* The `root` module in the `dev` directory
* The `image` module in the modules folder
* The `container` module in the modules folder
* The `volume` module as a nested module of the container module

It is best practice that the root module the place is where your changes should go. Change the other modules as sparingly as possible.

# Prerequisites

* Linux
* Docker
* Terraform
* For docker volume backups: a user with passwordless sudo access (ALL = (ALL) NOPASSWD:ALL)

NOTE: Will not work on Docker Desktop for Windows or Mac.

# Getting Started

Switch to the `dev` directory and see what will deployed:

```
$ terraform plan
```

And deploy it:

```
$ terraform apply
```

In the default setup this will provision the following resources:

```
module.container["grafana"].docker_container.app_container[0]
module.container["grafana"].docker_container.app_container[1]
module.container["grafana"].random_string.random[0]
module.container["grafana"].random_string.random[1]
module.container["nginx"].docker_container.app_container[0]
module.container["nginx"].docker_container.app_container[1]
module.container["nginx"].random_string.random[0]
module.container["nginx"].random_string.random[1]
module.image["grafana"].docker_image.container_image
module.image["nginx"].docker_image.container_image
module.container["grafana"].module.volume[0].docker_volume.container_volume[0]
module.container["grafana"].module.volume[0].docker_volume.container_volume[1]
module.container["grafana"].module.volume[1].docker_volume.container_volume[0]
module.container["grafana"].module.volume[1].docker_volume.container_volume[1]
module.container["nginx"].module.volume[0].docker_volume.container_volume[0]
module.container["nginx"].module.volume[1].docker_volume.container_volume[0]
```

# Build and Test

A couple of easy tips to build upon this template.

## Add or remove a container of the same type

Let's assume you need an extra Nginx container. I've used the amount of external ports as a count for the amount of containers. In this way Terraform will never try to deploy more containers than there are ports. Adding a container is a breeze:

1. Add an external port to the root `terraform.tfvars` file (normally excluded from git!)
2. Run a terraform plan
3. Run a terraform apply
4. Curl your new container(s) on localhost:newport

Do the opposite to reduce the amount of containers.

## Add (or remove) a different type of container

Let's assume you need a new deployment of Apache, 2 containers. This is some more work, but still no rocket science. You can copy most of the stuff from the example `nginx` and `grafana` configs.

1. Add an approriate block in the `locals.tf` file. For Apache it would look like this:

```
    httpd = {
      # These keys will be used later as reference
      container_count = length(var.ext_port["httpd"][var.env])
      image           = var.image["httpd"][var.env]
      int             = 80
      ext             = var.ext_port["httpd"][var.env]
      # For our dynamic block we're creating a list with maps in there, so that we can create multiple volumes when needed
      volumes = [
        { container_path_each = "/usr/local/apache2" }
      ]
    }
```

2. Add an appropriate block in the `terraform.tfvars` file. For example:

```
  httpd = {
    dev = [7070, 7171]
    prd = [1770, 1771]
  }
```

3. Add an appropriate block in the `variables.tf` file to pull the correct images per environment. For example:

```
    httpd = {
      dev = "httpd:latest"
      prd = "httpd:alpine"
    }
```

4. Run a terraform plan
5. Run a terraform apply
6. Curl your new container(s) on localhost:newport

Do the opposite to remove a type of container.

## Add or remove a persistent volume

Let's assume our 2 new Apache containers need extra persistent storage, say a data folder. It's probably self-explanatory up till this point and also easy, since we only need to edit the `locals.tf` file:

1. Add the location where the persistent volume shoud be created at the volumes array. For example:

```
      volumes = [
        { container_path_each = "/usr/share/nginx/html" },
        { container_path_each = "/mnt/data" }
      ]
```

2. Run a terraform plan
3. Run a terraform apply

When you have a completely stateless container, not in the need of a persistent storage, you just create a blank map like so:

```
      volumes = {}
```

## Note on persistent volume backups

With every `terraform destroy`, our persistent volumes will get destroyed as well. You can protect these resources, but Terraform will error out, so not something you want.

The volumes are probably persistent for a reason, that's why they're being backupped at destroy time, using provisioners (a Terraform 'last resort' as they call them).

The backups are being written just outside the Terraform folder, outside the repo. Technical details can be found in the nested volume module.

## Creating a complete different environment

Probably the most awesome of them all and also the most easy: duplicating your `dev` environment, creating a `prd` environment, that is fully operational within seconds.

1. Duplicated the `dev` folder to `prd`
2. Move to the `prd` folder
3. Delete the `terraform.tfstate` files
4. Rename the `dev` variable to `prd` in the `variables.tf`
5. Run a terraform plan
6. Run a terraform apply
7. Curl your new container(s) on localhost:newport

### Author
Henk Batelaan - iohenkies@devops.tf
