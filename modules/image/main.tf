# === image/main.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# Made this one more general with the for_each function, proper local and variables
resource "docker_image" "container_image" {
  name = var.image_in
}
