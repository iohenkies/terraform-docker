# === container/outputs.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# I've cleaned this up a lot while making the code more DRY
output "app_access" {
  # The => operator creates a map of my containers and ip:port
  value = { for i in docker_container.app_container[*] : i.name => join(":", [i.ip_address], i.ports[*]["external"]) }
}
