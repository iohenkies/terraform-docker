# === container/volume/providers.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# Source has been changed since version 0.14
terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
    }
  }
}
