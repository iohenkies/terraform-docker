# === container/volume/variables.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# These variables need to be declared to tie them to our root module, where they are defined in our root module blocks (linked to our local.deployment)
variable "volume_count" {}
variable "volume_name" {}
