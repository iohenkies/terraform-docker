# === container/volume/outputs.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# I've cleaned this up a lot while making the code more DRY

output "volume_output" {
  value = docker_volume.container_volume[*].name
}
