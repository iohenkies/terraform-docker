# === container/volume/main.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# In this setup every container gets a docker volume
resource "docker_volume" "container_volume" {
  count = var.volume_count
  # We need to match our volume names with container names, hence the interpolation and creating an implicent dependency
  name = "${var.volume_name}_${count.index}"
  lifecycle {
    # This can be set to true to prevent destruction of the volume (data). It errors our Terraform though. On Linux hosts, we can make some backups instead
    prevent_destroy = false
  }
  # The below only works on Linux hosts and for a user with passwordless sudo access (ALL = (ALL) NOPASSWD:ALL)
  # First provisioner for our volume backups
  provisioner "local-exec" {
    # A destroy-time provisioner to ensure the backup directory
    when    = destroy
    command = "mkdir ${path.cwd}/../../backup/"
    # We need a continue here since we're dealing with multiple containers that will, except for the first one, error out
    on_failure = continue
  }
  # Second provisioner for our volume backups
  provisioner "local-exec" {
    # A destroy-time provisioner to create the actual backup
    when    = destroy
    command = "sudo tar -czf ${path.cwd}/../../backup/${self.name}.tgz ${self.mountpoint}/"
    # When the backup fails, we need an error this time
    on_failure = fail
  }
}
