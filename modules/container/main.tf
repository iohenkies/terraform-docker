# === container/main.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# The random string is to make container names unique, when deploying multiple containers
resource "random_string" "random" {
  # We're letting the actual count depend on a local that depends on the amount of specified ports
  count   = var.count_in
  length  = 4
  upper   = false
  special = false
}

# Variable placeholders for our containers. Variables defined in our root module blocks
resource "docker_container" "app_container" {
  count = var.count_in
  name  = join("-", [var.name_in, var.env_in, random_string.random[count.index].result])
  image = var.image_in
  ports {
    internal = var.int_port_in
    external = var.ext_port_in[count.index]
  }
  # Volumes can be tighly coupled like this (same lifecycle as container) or loosely coupled (with a seperate module)
  # To be able to create multiple volumes per container in a dynamic environment, we need a dynamic block and a nested module
  dynamic "volumes" {
    for_each = var.volumes_in
    content {
      container_path = volumes.value["container_path_each"]
      # Referencing our volume resource specified in the volume module
      volume_name = module.volume[count.index].volume_output[volumes.key]
    }
  }
}

# The nested volum module as explained above
module "volume" {
  source = "./volume"
  # How many times should this module run (should be the same as the container count)
  count = var.count_in
  # How many times will a volume be created, every time this modules is run
  volume_count = length(var.volumes_in)
  volume_name  = "${var.name_in}_${var.env_in}_${random_string.random[count.index].result}_volume"
}
