# === container/variables.tf ===
#
# When everything is set, the modules are as static as possible and are only changed with big organisational changes

# These variables need to be declared to tie them to our root module, where they are defined in our root module blocks (linked to our local.deployment)
variable "env_in" {}
variable "name_in" {}
variable "image_in" {}
variable "int_port_in" {}
variable "ext_port_in" {}
variable "volumes_in" {}
variable "count_in" {}
